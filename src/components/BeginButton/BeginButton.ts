// import { RouteUrl } from '../constants/routes';
import './BeginButton.css';
import { IBeginButtonProps } from './BeginButton.type';

function BeginButton(props: IBeginButtonProps) {
    const beginButton = document.createElement('div');
    beginButton.className = 'beginButton';
    beginButton.innerHTML = props.name;
    const aButton = document.createElement('a');
    aButton.className = 'aButton';
    // aButton.href = RouteUrl.Characters;
    aButton.href = props.hrefURL;
    aButton.append(beginButton);
    return aButton;
}

export default BeginButton;