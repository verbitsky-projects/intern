export interface IBeginButtonProps {
    name: string;
    hrefURL: string;
}