import './Header.css';
import Navbar from '../Navbar/Navbar'
import logoSrc from '../../assets/logo.png'
function Header() {

    const header = document.createElement('header');
    const logo = document.createElement('img');
    logo.style.marginLeft = '30px';
    logo.src = logoSrc;
    header.className = 'header';
    header.append(logo);
    header.append(Navbar());
    return header;
}

export default Header;