import { RouteUrl } from "../constants/routes";
import ErrorPage from "../pages/errorPage/ErrorPage";
import Home from "../pages/Home/Home";

function Router() {
    switch (window.location.pathname) {
        case RouteUrl.Home:
            return Home();
        default:
            return ErrorPage();
    }
}

export default Router;