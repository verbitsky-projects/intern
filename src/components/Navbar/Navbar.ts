import { RouteUrl } from '../constants/routes';
import './Navbar.css';
import NavbarItem from './NavbarItem/NavbarItem';
import { INavbarItemProps } from './NavbarItem/NavbarItem.type';

const buttons: INavbarItemProps[] = [
    {
        label: "Главная",
        href: RouteUrl.Home
    },
    {
        label: "Персонажи",
        href: RouteUrl.Characters
    }
]

function Navbar() {
    const navbar = document.createElement('div');
    navbar.className = 'navbar';
    buttons.forEach((button) => {
        navbar.append(NavbarItem(button));
    });
    return navbar;
}

export default Navbar;
