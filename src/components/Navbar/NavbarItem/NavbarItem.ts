import './NavbarItem.css';
import { INavbarItemProps } from './NavbarItem.type';

function NavbarItem(props: INavbarItemProps) {
    const navbarItem = document.createElement('div');

    navbarItem.className = 'navbarItem';
    navbarItem.innerHTML = `<a class='aNavItem' href="${props.href}">${props.label}</a>`;

    return navbarItem;
}

export default NavbarItem;