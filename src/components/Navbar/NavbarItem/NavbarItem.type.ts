export interface INavbarItemProps {
    label: string;
    href: string
}