import { RouteUrl } from '../../constants/routes';
import BeginButton from '../../BeginButton/BeginButton';
import './ErrorContent.css'
import { IErrorContentProps } from './ErrorContent.type';

function ErrorContent(props: IErrorContentProps) {
    const errorContent = document.createElement('div');
    errorContent.className = 'errorContent';
    errorContent.innerHTML = `<h1 class=errorContentH>${props.errorName}</h1><h3 class=errorContentH>${props.errorMessage}</h3>`;

    const beginButton = BeginButton({ name: 'На главную', hrefURL: RouteUrl.Home });
    beginButton.style.display = 'inline-block';
    beginButton.style.marginTop = '40px';

    errorContent.append(beginButton);

    return errorContent;
}

export default ErrorContent;