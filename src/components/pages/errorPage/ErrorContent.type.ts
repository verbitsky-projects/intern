export interface IErrorContentProps {
    errorName: string;
    errorMessage: string;
}