import Content from "../../Content/Content";
import backgroundURL from "../../../assets/404_1.png"
import Stabilizer from "../Home/Content/Stabilizer";
import ErrorContent from "./ErrorContent";


function ErrorPage() {
    const errorContent = ErrorContent({ errorName: '404', errorMessage: 'Ошибка 404. Такая страница не существует либо она была удалена' });

    return Content({ children: [errorContent], backgroundURL })
}

export default ErrorPage
