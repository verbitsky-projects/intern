import BeginButton from "../../../BeginButton/BeginButton";
import './Stabilizer.css'
import { RouteUrl } from '../../../constants/routes';

function Stabilizer(hText: string, messageText: string) {
    const stabilizer = document.createElement('div');
    stabilizer.className = 'stabilizer';
    stabilizer.innerHTML = `<h1>${hText}</h1><h3>${messageText}</h3>`;
    stabilizer.append(BeginButton({ name: 'Начать', hrefURL: RouteUrl.Characters }));
    return stabilizer;
}

export default Stabilizer;