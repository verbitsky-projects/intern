import Content from "../../Content/Content";
import Stabilizer from "./Content/Stabilizer";
import backgroundURL from "../../../assets/bg_1.png"

function Home() {
    const stabilizer = Stabilizer('Найди любимого <br> персонажа <br> “Гарри Поттера”', 'Вы сможете узнать тип героев, их <br> способности, сильные стороны и недостатки.')
    return Content({ children: [stabilizer], backgroundURL })
}

export default Home
