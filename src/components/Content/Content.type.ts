export interface IContentProps {
    children: string[] | Node[];
    backgroundURL: string;
}