import './Content.css';
// import bgSrc from '../../../../assets/bg_1.png';
// import BeginButton from './BeginButton';
import Header from '../Header/Header';
// import Stabilizer from './Stabilizer';
import { IContentProps } from './Content.type';

function Content(props: IContentProps) {
    const content = document.createElement('div');
    content.className = 'content';

    // push to css file
    content.style.backgroundImage = `url(${props.backgroundURL})`;
    content.style.backgroundSize = 'cover';
    content.style.backgroundRepeat = 'no-repeat';
    content.style.backgroundAttachment = 'fixed';
    content.style.backgroundPositionY = '76px'

    content.append(Header());

    props.children.forEach((child: string | Node) => {
        content.append(child)
    })



    return content;
}
export default Content;