module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    ignorePatterns: ['.eslintrc.js', 'webpack.config.js'],
    // Пресет с настройками. prettier должен быть последним.
    // Он удаляет некоторые правила eslint из-за которых могут возникать конфликты. */
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'prettier',
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: './tsconfig.json',
        tsconfigRootDir: __dirname,
        ecmaVersion: 12,
        sourceType: 'module',
    },
    // здесь мы подключаем плагины
    plugins: ['@typescript-eslint', 'prettier'],
    // Здесь переопределяются правила плагинов
    rules: {
        curly: 'error',
        quotes: [2, 'single', { avoidEscape: true }],
        '@typescript-eslint/interface-name-prefix': 0,
        '@typescript-eslint/no-unnecessary-type-assertion': 'warn',
        '@typescript-eslint/no-unsafe-assignment': 'warn',
        '@typescript-eslint/no-non-null-assertion': 'error',
        '@typescript-eslint/no-inferrable-types': 0,
        '@typescript-eslint/no-empty-interface': 0,
        '@typescript-eslint/no-explicit-any': 0,
        '@typescript-eslint/unbound-method': 0,
        '@typescript-eslint/member-ordering': 'warn',
        '@typescript-eslint/explicit-function-return-type': 'warn',
        '@typescript-eslint/no-magic-numbers': [
            'error',
            {
                ignoreNumericLiteralTypes: true,
                ignoreEnums: true,
                enforceConst: true,
                ignoreReadonlyClassProperties: true,
                ignore: [0, 1, 2, 12, 24, 60, 100, 1000, 2000, 10000],
            },
        ],
        '@typescript-eslint/naming-convention': [
            'error',
            {
                selector: 'default',
                format: ['camelCase'],
            },
            {
                selector: 'variable',
                format: ['camelCase', 'UPPER_CASE'],
            },
            {
                selector: 'parameter',
                format: ['camelCase'],
                leadingUnderscore: 'allow',
            },
            {
                selector: 'memberLike',
                modifiers: ['private'],
                format: ['camelCase'],
                leadingUnderscore: 'require',
            },
            {
                selector: 'method',
                format: ['camelCase'],
            },
            {
                selector: 'enumMember',
                format: ['PascalCase', 'UPPER_CASE'],
            },
            {
                selector: 'typeLike',
                format: ['PascalCase'],
            },
        ],
    },
};
